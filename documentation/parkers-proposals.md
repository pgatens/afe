
# Introduction #

So this is just a simple documentation file I made to write down a few things that are going through my head.  
The first thing I plan on doing with it is listing all of the classes I think need to exist.  
Then I will list the methods that I think should be in the classes.  

**NOTE:** This is completely incomplete. These are just things that popped into my head on the spot. More to be added.

# Classes #

* Alien (object)
	+ Slime (alien)
		- Camouflage Slime (slime)
		- Sticky Slime (slime)
	+ Jelly (alien)
		- Messy Jelly (jelly)
		- Moulded Jelly (jelly)
		- Trifle Jelly (jelly)
	+ Blob (alien)
		- Blue Blob (blob)
		- Green Blob (blob)
		- Rainbow Blob (blob)
		- Red Blob (blob)
		- White Blob (blob)

* Tank (set)

* Freezer (set)

* Disease (object)
	+ Cancer (disease)
	+ Anorexia (disease)
	+ Sterile (disease)
	
* Store (object)

* Player (object) [or some kind of "main" class]

## Methods ##

* Alien
	+ Breed
	+ Die

* Tank
	+ Feed Tank
		- Feed Blobs
		- Feed Bigger Aliens (Jelly/Slime)

* Freezer
	+ Freeze Aliens
	+ Unfreeze Aliens
	
* Disease
	+ Heal Disease
	+ Spread Disease
	+ Perform Symptom

* Store
	+ Buy Aliens
	+ Sell Aliens

* Player/Main
	+ New Game
	+ Save Game
	+ Load Game
	+ Money Handler / Food Amount Handler (Possibly done in tank)
