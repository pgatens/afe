"""
This file is part of AFE.

AFE is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

AFE is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with AFE.  If not, see <http://www.gnu.org/licenses/>.
    
@package AFE.aliens.basealien
@author: Peter Gatens
@version: 0.1.0

Describes a stereotypical alien in our system. The base class for all aliens.
"""

class BaseAlien(object):
  """
  Class shared by all aliens in the tank
  Holds most of the aliens attributes
  @author Peter Gatens
  @author Parker Sprouse
  """

  def __init__(self, genus, species=None):
    """
    Constructor. Sets up the alien
    @param self - explicit self
    @param genus - the genus of the alien
    @param species - the species of the alien
    """
    self.genus = genus
    self.species = species
    self.age = 0
    self.maxAge = 0 
    self.disease = []
    self.health = 100
   
  def getGenus(self):
    """
    Returns the genus of the alien this is which only works
    on specific implementation of a single alien. 
    """
    # TODO: Error checking so superclasses can't call this
    return self.getSpecies().__bases__[0]
    
  def getSpecies(self):
    """
    Returns the class of alien this is, which is the species.
    Only works on specific species of aliens (which should be the only ones
    instansiated anyway).
    """
    # TODO: Some error checking so superclasses can't call this
    return self.__class__
  
  def getGenusName(self):
    return self.genus
  
  def getSpeciesName(self):
    return self.species
        
  # TODO: needs to be unique 
  def __str__(self): 
    """
    Returns a string representation of the base alien class
    """
    return "%s : %s : %s" % (self.genus, self.species, self.age)
