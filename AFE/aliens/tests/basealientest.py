"""
This file is part of AFE.

AFE is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

AFE is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with AFE.  If not, see <http://www.gnu.org/licenses/>.
    
@package AFE.aliens.tests.basealientest
@author: Peter Gatens
@version: 0.1.0

Test the properties in BaseAlien including the genera they contain and the species in that genera
"""

import unittest
from AFE.util.alienutils import AlienUtils

class TestAllAliens(unittest.TestCase):

  def setUp(self):
    """ Called when each test is executed to create all the aliens we know of """ 
    AlienUtils.generateAliens()
    
  def testGeneraSize(self):
    """ 
    Getting the size of the known alien dictionary and making sure there are the same
    number present as actual objects in the BaseAlien class
    """
    self.assertEquals(len(AlienUtils.knownTypes), AlienUtils.numberOfGenus())
      
  def testGenusTypes(self):
    """
    Checks the type of genus is present and has the correct name in the list of genera
    """
    for genusName in AlienUtils.getAllGenusNames():
      self.assertTrue(genusName in AlienUtils.knownTypes, genusName + " is not in the list of genera.")
      
      
  def testSpeciesTypes(self):
    """
    Checks each of the species and ensures they are present and have the 
    correct name and placement in the list of species in AlienUtils
    """
    for genus in AlienUtils.getAllGenus():
      speciesNameList = AlienUtils.getAllSpeciesNames(genus)
      for speciesName in speciesNameList:
        baseSpeciesNames = AlienUtils.getSpeciesNames(genus().getGenusName())
        self.assertTrue(speciesName in baseSpeciesNames, speciesName + " is not in the list of aliens.")
       
