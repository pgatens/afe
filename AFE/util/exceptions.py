"""
This file is part of AFE.

AFE is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

AFE is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with AFE.  If not, see <http://www.gnu.org/licenses/>.
    
@package AFE.util.exceptions
@author: Peter Gatens
@version: 0.1.0

Class of custom exceptions to better describe error conditions within AFE
"""

class MyException(object):
  
  def __init__(self):
    pass
        