"""
This file is part of AFE.

AFE is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

AFE is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with AFE.  If not, see <http://www.gnu.org/licenses/>.
    
@package AFE.util.AlienUtils
@author: Peter Gatens
@version: 0.1.0

Functions to assist creating a modifying aliens but aren't related to a single instance of an alien. 
"""

from AFE.aliens.basealien import BaseAlien

class AlienUtils(object):
  """ Utilities to work with aliens. Static methods only here """ 
  
  """
  The list of all the know types of alien - we use this list to generate all types everywhere 
  This list is more important than it once was, given this list it actually uses it to create
  the genera and classes in the system. Gives us great flexibilty and now we don't have to clutter
  up aliens with hundreds of subclasses.
  
  Example:
  
    "Alien" : {"A", "B", "C" },
    
  As an entry in the list will generate Genus "Alien" a subclass of BaseAlien
  with subclasses "AAlien", "BAlien" and "CAlien" just from the list. These can be used as any alien
  could be in the system when it was hardcoded.
  """
  knownTypes = {
                "Blob" : {"Red", "Blue", "Green", "White", "Rainbow"},
                "Jelly": {"Messy", "Moulded", "Trifle"},
                "Slime": {"Camouflage", "Sticky"},
                }
  
  # A dictionary map of String -> Class for genera generated (see AlienUtils.getGenus() how to access these)
  alienGenera = {}
  # A dictionary map of String -> Class for species generated (see AlienUtils.getSpecies() how to access these)
  alienSpecies = {}
  
  @staticmethod
  def generateAliens():
    # Only generate aliens if there are no genera or species already generated (to prevent doing it twice)
    if not(AlienUtils.alienGenera and AlienUtils.alienSpecies):
      for genus in AlienUtils.knownTypes:
        GenusClass = AlienUtils.__generateGenus(genus)
        AlienUtils.alienGenera[genus] = GenusClass
        speciesList = AlienUtils.knownTypes.get(genus, None)
        for species in speciesList:
          SpeciesClass = AlienUtils.__generateSpecies(GenusClass, genus, species)
          AlienUtils.alienSpecies[species+genus] = SpeciesClass

  @staticmethod
  def __generateGenus(genusName):
    """ 
    Creates a Genus class, which is a subclass of BaseAlien, in the system without having to create the class.
    """
    def genusInit(self, speciesName=None):
      BaseAlien.__init__(self, genusName, speciesName)   
    return type(genusName, (BaseAlien,), {'__init__': genusInit}) 
  
  @staticmethod
  def __generateSpecies(GenusClass, genusName, speciesName):
    """
    Given a Genus in the system this method creates a direct subclass of this genus
    which is a Species in the system. No need to hardcode any species
    """ 
    def speciesInit(self):
      GenusClass.__init__(self, speciesName + " " + genusName)
    return type(genusName, (GenusClass, ), {'__init__': speciesInit})
  
  @staticmethod
  def getSpecies(speciesName):
    """
    Now, all the aliens are stored in a dictionary. To get a class called "BlueBlob" a call to
      AlienUtils.getSpecies("BlueBlob")
    will return the BlueBlob *class* if it exists. This is NOT instansiated yet. 
      AlienUtils.getSpecies("BlueBlob")()
    Creates an instance of a new BlueBlob (assuming is exists). Note the brackets afterwards.
    """
    species = AlienUtils.alienSpecies.get(speciesName)
    if species is not None:
      return species
    raise Exception(speciesName + " is not a valid species!")
  
  @staticmethod
  def getGenus(genusName):
    """
    Accessed in an identical way to AlienUtils.getSpecies(...) accessed by Genus name. 
    To instansiate a genus called "Slime" for example call:
      AlienUtils.getGenus("Slime")()
    """
    genus = AlienUtils.alienGenera.get(genusName)
    if genus is not None:
      return genus
    raise Exception(genusName + " is not a valid genus!")
    
  @staticmethod
  def getSpeciesNames(genusName):
    """ Returns a formatted list of the species names for a given genus """
    speciesNameList = []
    for speciesName in AlienUtils.knownTypes.get(genusName, []):
      speciesNameList.append(speciesName + " " + genusName);
    return speciesNameList
  
  @staticmethod
  def getAllGenus():
    """ Returns all genera in the system """
    return BaseAlien.__subclasses__()  
    
  @staticmethod
  def numberOfGenus():
    """ Returns how many genera exist """
    return len(BaseAlien.__subclasses__()) 
    
  @staticmethod
  def getAllSpecies(genusClass):
    """ 
    Given a genus return a list of classes that relate to the species in this genus 
    """
    try:
      return genusClass.__subclasses__()
    except AttributeError:
      print("Can't get the species for " + genusClass + "\n" + 
      "Check it's a valid genus which is the superclass of the species you are looking for")   
      
  @staticmethod
  def getAllGenusNames():
    return [g().getGenusName() for g in AlienUtils.getAllGenus()]
  
  @staticmethod
  def getAllSpeciesNames(species):
    return [s().getSpeciesName() for s in AlienUtils.getAllSpecies(species)]