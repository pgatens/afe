"""
This file is part of AFE.

AFE is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

AFE is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with AFE.  If not, see <http://www.gnu.org/licenses/>.
    
@package AFE.containers.container
@author: Peter Gatens
@version: 0.1.0
"""

from collections import defaultdict
from AFE.util.alienutils import AlienUtils

class Container(defaultdict):
  """
  Contains all instances of the Aliens that exist and contains methods to interact with them
  @author Peter Gatens
  @author Parker Sprouse
  """
  
  def __init__(self):
    """
    Tank constructor - creates a defaultdictionary to be used as the tank
    @param self - required self parameter
    """
    super(Container, self).__init__(dict)
    self.__setUpContainer()
    
  def __setUpContainer(self):
    """
    Private method to set up the container so every known
    genus and species has a container and the set of all
    instansated aliens can be accessed by self[genus][species]
    """
    genera = AlienUtils.getAllGenus();
    for genus in genera:
      self[genus] = {}
      speciesDict = {}
      for species in AlienUtils.getAllSpecies(genus):
        speciesDict[species] = []
      self[genus] = speciesDict
           
  def addToContainer(self, *aliens):
    """
    Adds aliens to the container.
    Accepts single aliens, multiple aliens or a unpacked tuple/list of multiple aliens to add.
    
    Valid syntax:
    tank = Tank()
    tank.add(alien1)
    tank.add(alien1, alien2, alien3)
    tank.add(*[alien1, alien2, alien3])
    tank.add(*(alien1, alien2, alien3)]
    
    Where alienN is any type of alien in the system.
    
    @param self - explicit self parameter
    @param aliens - aliens to be added to the container
    """
    for alien in aliens:
      # The specific type (RedBlob, BlueBlob, MessyJelly etc)
      alienSpecies = alien.getSpecies()
      # Get the first (and only) superclass (Blob, Slime, Jelly etc)
      alienGenus = alien.getGenus() # TODO: check __bases__ has at least one item 
      speciesDict = self.get(alienGenus)
      # Safety, so we don't give it invalid stuff even though we'll check 
      if speciesDict is not None:
        # Don't add the alien to the tank if it already in the container (because that would be silly)
        if alien not in speciesDict[alienSpecies]:
          # Add the alien to the right species list
          speciesDict[alienSpecies].append(alien)
      # TODO: exception here if speciesDict is empty
    
  def removeFromContainer(self, *aliens):
    """
    Remove the alien from the tank set
    @param self - required self parameter
    @param alien - alien to be removed from the container
    """
    for alien in aliens:
      alienGenus = alien.getGenus()
      alienSpecies = alien.getSpecies()
      speciesDict = self.get(alienGenus)
      if speciesDict is not None:
        # If the alien is actually there 
        singleSpeciesList = speciesDict[alienSpecies]
        if alien in singleSpeciesList:
          singleSpeciesList.remove(alien)
        # TODO: Exception when trying to remove an alien that isn't even in the container
          
    
  def totalNumberOfAliens(self):
    """
    Returns the total number of aliens in the container.
    """
    total = 0
    for genus in self:
      for species in self[genus]:
        total += len(self[genus][species])
    return total
  
  def numberOfGenera(self):
    """
    Returns the number of unique genera in the container.
    """
    # Returns a "1" if there are any species with members in the current genus
    # sums all the "1"s up to get how many genera have members
    return sum([1 for genus in self if self.numberOfSpecies(genus) is not 0]) 
  
  def numberOfSpecies(self, genus):
    """
    Returns the number of unique species that exist in the container
    given the genus.
    """
    total = 0
    speciesDict = self.get(genus, [])
    for species in speciesDict:
      if speciesDict[species]:
        total += 1
    return total