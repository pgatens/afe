"""
This file is part of AFE.

AFE is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

AFE is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with AFE.  If not, see <http://www.gnu.org/licenses/>.
    
@package AFE.containers.tank.tank
@author: Parker Sprouse
@author: Peter Gatens
@version: 0.1.0
"""

from AFE.containers.container import Container

class Tank(Container):
  """
  A container that is the current state of the player's tank, with all the aliens the player owns.
  Extends a generic contain that is used for the Tank and the Freezer but adds tank like methods
  to do with feeding/killing/aging etc
  """
  
  def __init__(self):
    """
    Tank constructor - creates a Tank, calls the superclass contructor of Container
    to add all the relevant methods for dealing with aliens.
    """
    super(Tank, self).__init__()
    