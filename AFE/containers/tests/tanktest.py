"""
This file is part of AFE.

AFE is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

AFE is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with AFE.  If not, see <http://www.gnu.org/licenses/>.
    
@package AFE.containers.tank.tests.tankTest
@author: Peter Gatens
@author: Parker Sprouse
@version: 0.1.0

Tests different methods of adding and removing from the tank making sure all the aliens are in the right place
"""

import unittest

from AFE.containers.tank import Tank
from AFE.util.alienutils import AlienUtils


class TankTest(unittest.TestCase):
  
  def setUp(self):
    # Make sure all the aliens exist (good thinkin')
    AlienUtils.generateAliens()
    # Get some genera to use in this class later
    self.blobClass = AlienUtils.getGenus("Blob")
    self.jellyClass = AlienUtils.getGenus("Jelly")
    self.slimeClass = AlienUtils.getGenus("Slime")
    
  def testAddingAliens(self):
    # Make some aliens
    rb = AlienUtils.getSpecies("RedBlob")()
    bb = AlienUtils.getSpecies("BlueBlob")()
    wb1 = AlienUtils.getSpecies("WhiteBlob")()
    wb2 = AlienUtils.getSpecies("WhiteBlob")()
    # Create the tank
    tank = Tank()
    # Add all the aliens to the tank
    tank.addToContainer(rb)
    tank.addToContainer(bb)
    tank.addToContainer(wb1)
    tank.addToContainer(wb2)
    self.assertEqual(1, tank.numberOfGenera())
    self.assertEqual(3, tank.numberOfSpecies(rb.getGenus()))
    # Now make some other genera and species
    mj = AlienUtils.getSpecies("MessyJelly")()
    tj = AlienUtils.getSpecies("TrifleJelly")()
    ss = AlienUtils.getSpecies("StickySlime")()
    tank.addToContainer(mj)
    tank.addToContainer(tj)
    # Now another genus and 2 more species added
    self.assertEqual(2, tank.numberOfGenera())
    self.assertEqual(3, tank.numberOfSpecies(self.blobClass)) # Blob
    self.assertEqual(2, tank.numberOfSpecies(self.jellyClass)) # Jelly
    self.assertEqual(0, tank.numberOfSpecies(self.slimeClass)) # Slime (not there) 
    # Add the slime!
    tank.addToContainer(ss)
    # Another genus and another species added
    self.assertEqual(3, tank.numberOfGenera())
    self.assertEqual(3, tank.numberOfSpecies(self.blobClass)) # Blob
    self.assertEqual(2, tank.numberOfSpecies(self.jellyClass)) # Jelly
    self.assertEqual(1, tank.numberOfSpecies(self.slimeClass)) # Slime 
    # Check all the aliens are in the right place
    
    # Blobs
    self.assertEqual([rb], tank[self.blobClass][rb.getSpecies()])
    self.assertEqual([bb], tank[self.blobClass][bb.getSpecies()])
    self.assertEqual([wb1, wb2], tank[self.blobClass][wb1.getSpecies()])
    # Jellies
    self.assertEqual([mj], tank[self.jellyClass][mj.getSpecies()])
    self.assertEqual([tj], tank[self.jellyClass][tj.getSpecies()])
    # Slimes
    self.assertEqual([ss], tank[self.slimeClass][ss.getSpecies()])
    
  def testAddingMethods(self):
    """
    Aliens can be added in several different ways - so check they are equivalent
    """
    tank1 = Tank()
    tank2 = Tank()
    tank3 = Tank()
    
    # Make some nice aliens
    rb = AlienUtils.getSpecies("RedBlob")()
    bb1 = AlienUtils.getSpecies("BlueBlob")()
    bb2 = AlienUtils.getSpecies("BlueBlob")()
    bb3 = AlienUtils.getSpecies("BlueBlob")()
    wb1 = AlienUtils.getSpecies("WhiteBlob")()
    wb2 = AlienUtils.getSpecies("WhiteBlob")()
    rab = AlienUtils.getSpecies("RainbowBlob")()
    cs1 = AlienUtils.getSpecies("CamouflageSlime")()
    cs2 = AlienUtils.getSpecies("CamouflageSlime")()
    ss = AlienUtils.getSpecies("StickySlime")()
    mj = AlienUtils.getSpecies("MessyJelly")()
    mmj = AlienUtils.getSpecies("MouldedJelly")()
    tj = AlienUtils.getSpecies("TrifleJelly")()
    
    # Add them one by one
    tank1.addToContainer(rb)
    tank1.addToContainer(bb1)
    tank1.addToContainer(bb2)
    tank1.addToContainer(bb3)
    tank1.addToContainer(wb1)
    tank1.addToContainer(wb2)
    tank1.addToContainer(rab)
    tank1.addToContainer(cs1)
    tank1.addToContainer(cs2)
    tank1.addToContainer(ss)
    tank1.addToContainer(mj)
    tank1.addToContainer(mmj)
    tank1.addToContainer(tj)
    
    # Add them all as a comma separated list
    tank2.addToContainer(rb, bb1, bb2, bb3, wb1, wb2, rab, cs1, cs2, ss, mj, mmj, tj)
    
    # Add them all as an unpacked single list
    tank3.addToContainer(*[rb, bb1, bb2, bb3, wb1, wb2, rab, cs1, cs2, ss, mj, mmj, tj])
    
    # Now check all 3 are equivalent
    
    # Tank 1
    self.assertEqual(3, tank1.numberOfGenera())
    self.assertEqual(4, tank1.numberOfSpecies(self.blobClass)) # Blob
    self.assertEqual(3, tank1.numberOfSpecies(self.jellyClass)) # Jelly
    self.assertEqual(2, tank1.numberOfSpecies(self.slimeClass)) # Slime 
    self.assertEqual(13, tank1.totalNumberOfAliens())
    
    # Tank 2
    self.assertEqual(3, tank2.numberOfGenera())
    self.assertEqual(4, tank2.numberOfSpecies(self.blobClass)) # Blob
    self.assertEqual(3, tank2.numberOfSpecies(self.jellyClass)) # Jelly
    self.assertEqual(2, tank2.numberOfSpecies(self.slimeClass)) # Slime 
    self.assertEqual(13, tank2.totalNumberOfAliens())
    
    # Tank 3
    self.assertEqual(3, tank3.numberOfGenera())
    self.assertEqual(4, tank3.numberOfSpecies(self.blobClass)) # Blob
    self.assertEqual(3, tank3.numberOfSpecies(self.jellyClass)) # Jelly
    self.assertEqual(2, tank3.numberOfSpecies(self.slimeClass)) # Slime 
    self.assertEqual(13, tank3.totalNumberOfAliens())
    
  def testRemovingAliens(self):
    rb = AlienUtils.getSpecies("RedBlob")()
    bb1 = AlienUtils.getSpecies("BlueBlob")()
    bb2 = AlienUtils.getSpecies("BlueBlob")()
    bb3 = AlienUtils.getSpecies("BlueBlob")()
    wb1 = AlienUtils.getSpecies("WhiteBlob")()
    wb2 = AlienUtils.getSpecies("WhiteBlob")()
    rab = AlienUtils.getSpecies("RainbowBlob")()
    cs1 = AlienUtils.getSpecies("CamouflageSlime")()
    cs2 = AlienUtils.getSpecies("CamouflageSlime")()
    ss = AlienUtils.getSpecies("StickySlime")()
    mj = AlienUtils.getSpecies("MessyJelly")()
    mmj = AlienUtils.getSpecies("MouldedJelly")()
    tj = AlienUtils.getSpecies("TrifleJelly")()
    
    tank = Tank()
    
    # Add them all to the tank
    tank.addToContainer(rb, bb1, bb2, bb3, wb1, wb2, rab, cs1, cs2, ss, mj, mmj, tj)
    # Initial set up    
    self.assertEqual(3, tank.numberOfGenera())
    self.assertEqual(4, tank.numberOfSpecies(self.blobClass)) # Blob
    self.assertEqual(3, tank.numberOfSpecies(self.jellyClass)) # Jelly
    self.assertEqual(2, tank.numberOfSpecies(self.slimeClass)) # Slime 
    self.assertEqual(13, tank.totalNumberOfAliens())
    
    # Four types of blob, remove the only RedBlob
    tank.removeFromContainer(rb)
    # One less species of blob - one less total aliens
    self.assertEqual(3, tank.numberOfGenera())
    self.assertEqual(3, tank.numberOfSpecies(self.blobClass)) # Blob
    self.assertEqual(3, tank.numberOfSpecies(self.jellyClass)) # Jelly
    self.assertEqual(2, tank.numberOfSpecies(self.slimeClass)) # Slime 
    self.assertEqual(12, tank.totalNumberOfAliens())
    
    # Different method, remove all of the slimes at once:
    tank.removeFromContainer(cs1, cs2, ss)
    # One less genera, 2 less slime species and 3 less total aliens
    self.assertEqual(2, tank.numberOfGenera())
    self.assertEqual(3, tank.numberOfSpecies(self.blobClass)) # Blob
    self.assertEqual(3, tank.numberOfSpecies(self.jellyClass)) # Jelly
    self.assertEqual(0, tank.numberOfSpecies(self.slimeClass)) # Slime 
    self.assertEqual(9, tank.totalNumberOfAliens())
    
    # Another different method remove another blob species and all of the jellys
    tank.removeFromContainer(*(rab, mj, mmj, tj))
    # One less genera, 3 less jelly species, 1 less blob species and 4 less total aliens
    self.assertEqual(1, tank.numberOfGenera())
    self.assertEqual(2, tank.numberOfSpecies(self.blobClass)) # Blob
    self.assertEqual(0, tank.numberOfSpecies(self.jellyClass)) # Jelly
    self.assertEqual(0, tank.numberOfSpecies(self.slimeClass)) # Slime 
    self.assertEqual(5, tank.totalNumberOfAliens())