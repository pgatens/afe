# AFE #

AFE is an alien breeding/strategy/simulation game written purely in Python. It is loosely based on the WAP mobile and interactive television game Alien Fish Exchange by NTL Interactive Cable. Sadly, almost all references to that game have vanished from the web. We will take the ideas from this game as a starting point and expand on it a lot more. The result will be a feature rich, cross-platform game that's hopefully as addictive as Alien Fish Exchange was!

* * * 

At the moment there is only two of us working on this project in its early stages when time permits. It's not much to look at at the moment, so keep checking back! It will be exciting to watch it grow. If you would like to help out, or ask a few questions about where we are headed don't hesitate to message me.

* * *
